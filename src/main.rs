//! Fetch and save new MangaDex API YAML docs.

use std::env;
use std::fs;
use std::io::prelude::*;
use std::path::Path;

use lazy_static::lazy_static;
use notify_rust::Notification;
use regex::Regex;
use reqwest::Client;
use semver::Version;
use yaml_rust::{Yaml, YamlLoader};

const BASE_URL: &str = "https://api.mangadex.org";
lazy_static! {
    /// Semantic versioning as specified in https://semver.org.
    ///
    /// This regular expression doesn't strictly check if the string is a pure SemVer string but if the string contains one.
    static ref SEMVER_REGEX: Regex = Regex::new(r"(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?").unwrap();
}

#[tokio::main]
async fn main() {
    let args: Vec<String> = env::args().collect();
    let dir = Path::new(if args.len() > 1 {
        args[1].as_str()
    } else {
        "./"
    });

    if let Err(e) = run(dir).await {
        use std::process;

        eprintln!("application error: {}", e);
        process::exit(1);
    }
}

/// Run the application.
async fn run(dir: &Path) -> anyhow::Result<()> {
    let client = Client::new();

    let local_version = get_local_version(dir)?;

    let (raw_doc, yaml_doc) = get_latest_mangadex_api_doc(&client).await?;
    let mangadex_version = parse_mangadex_version(&yaml_doc)?;

    // println!(
    //     "Fetched MangaDex version: {:?}\nLatest local version: {:?}",
    //     mangadex_version, local_version
    // );

    println!("{}", mangadex_version.to_string());

    if local_version.is_none() || mangadex_version > local_version.unwrap() {
        Notification::new()
            .summary("MangaDex API")
            .body(&format!(
                "MangaDex {} is available",
                mangadex_version.to_string()
            ))
            .show()?;

        save_doc(&mangadex_version, &raw_doc, dir)?;
    }

    Ok(())
}

/// Get the latest version number from the locally saved API docs.
fn get_local_version(dir: &Path) -> anyhow::Result<Option<Version>> {
    let mut latest_version = Version::new(0, 0, 0);
    let entries = fs::read_dir(dir)?;
    for entry in entries.flatten() {
        // Only process the file if it is a YAML file.
        match entry.path().as_path().extension() {
            Some(ext) => {
                if ext != "yml" && ext != "yaml" {
                    continue;
                }
            }
            None => continue,
        }

        // Parse the semantic version from the file name.
        if let Some(ver) = SEMVER_REGEX.find(entry.file_name().into_string().unwrap().as_str()) {
            let version = Version::parse(ver.as_str())?;

            latest_version = latest_version.max(version);
        }
    }

    let latest_version = if latest_version == Version::new(0, 0, 0) {
        None
    } else {
        Some(latest_version)
    };

    Ok(latest_version)
}

/// Get the latest version of the MangaDex API OpenAPI YAML doc.
async fn get_latest_mangadex_api_doc(client: &Client) -> anyhow::Result<(String, Yaml)> {
    let yaml_raw = client
        .get(format!("{}/api.yaml", BASE_URL))
        .send()
        .await?
        .text()
        .await?;

    let docs = YamlLoader::load_from_str(&yaml_raw)?;

    Ok((yaml_raw, docs[0].clone()))
}

/// Parse the current version of the MangaDex API from the OpenAPI YAML definition file.
fn parse_mangadex_version(doc: &Yaml) -> anyhow::Result<Version> {
    let version = Version::parse(&doc["info"]["version"].clone().into_string().unwrap())?;

    Ok(version)
}

/// Save the YAML doc to the local filesystem in the same specified directory.
fn save_doc(version: &Version, doc: &str, dir: &Path) -> anyhow::Result<()> {
    let new_file_name = format!("api-{}.yaml", version.to_string());
    let new_file_path = dir.join(Path::new(&new_file_name));
    // println!("Writing to {}", new_file_path.to_str().unwrap());
    fs::write(&new_file_path, &doc)?;

    // Save the new version to a simple text file to act as a changelog for manual review.
    let unchecked_versions_path = dir.join(Path::new("CHANGES"));
    let mut unchecked_versions_file = fs::OpenOptions::new()
        .create(true)
        .append(true)
        .open(unchecked_versions_path)?;
    unchecked_versions_file.write_all(format!("{}\n", version.to_string()).as_bytes())?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn regex_matches_valid_semver() {
        let tests = vec![
            "0.0.4",
            "1.2.3",
            "10.20.30",
            "01.1.1",
            "1.1.2-prerelease+meta",
            "1.1.2+meta",
            "1.0.0-alpha",
            "1.0.0-alpha.beta",
            "1.0.0-alpha.1",
            "1.0.0-alpha.0valid",
            "1.0.0-rc.1+build.1",
            "1.2.3-beta",
            "10.2.3-DEV-SNAPSHOT",
            "1.2.3-SNAPSHOT-123",
            "1.0.0",
            "2.0.0+build.1848",
            "2.0.1-alpha.1227",
            "1.0.0-alpha+beta",
            "1.2.3----RC-SNAPSHOT.12.9.1--.12+788",
            "1.2.3----R-S.12.9.1--.12+meta",
        ];

        for test in tests {
            assert!(SEMVER_REGEX.is_match(test))
        }
    }

    #[test]
    fn regex_doesnt_match_invalid_semver() {
        let tests = vec![
            "1",
            "1.2",
            // "01.1.1",
            // "9.8.7-whatever+meta+meta",
            // "1.2.3.DEV",
            // "1.2.3-0123",
            // "1.0.0-alpha_beta",
            "1.2-SNAPSHOT",
            // "1.2.31.2.3----RC-SNAPSHOT.12.09.1--..12+788",
        ];

        for test in tests {
            assert!(!SEMVER_REGEX.is_match(test))
        }
    }
}
