# mangadex-version-checker

This will fetch the latest MangaDex API doc and compare the version with
the locally saved YAML files. If they differ, the new docs will be saved
to the local filesystem as well as create and write a text file which
tracks which versions haven't been reviewed.

# Usage

```
mangadex-version-checker /path/to/mangadex/yaml/files
```
